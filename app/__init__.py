from flask import Flask
from flask import render_template
from flask_bootstrap import Bootstrap

app = Flask(__name__)
Bootstrap(app)

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/localizacion')
def localizacion():
    return render_template('localizacion.html')


@app.route('/colon')
def colon():
    return render_template('colon.html')


@app.route('/playas')
def playas():
    return render_template('playas.html')


@app.route('/comida')
def comida():
    return render_template('comida.html')


@app.route('/camara')
def camara():
    return render_template('camara.html')


if __name__== '__main__' :
    app.run(debug = True)